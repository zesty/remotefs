# remotefs

Symlink remote file (from http(s), ftp server, or any protocol supported by curl) to a virtual drive with persistent database storage

This project uses curl for requests and store symlink data with sqlite

This project is a fork of lionfs (https://github.com/pasqualirb/lionfs)

# Building
```
chmod +x compile.sh
./compile.sh
```

# Running
mounting folder:

`./remotefs vm_folder`

Creating a symlink:

`ln -s http://example.com/note.txt vm_folder/note.txt`