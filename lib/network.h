#ifndef NETWORK_H_
#define NETWORK_H_

#include <sys/time.h>
#include <sys/types.h>

typedef struct {
	long long size;
} lionfile_info_t;

int net_file_get_info(char*, lionfile_info_t*);
size_t net_file_get_data(char*, size_t, off_t, char*);

#endif
