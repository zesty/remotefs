#ifndef HTTP_H_
#define HTTP_H_

#include "network.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>

int http_file_get_info(char*, lionfile_info_t*);
size_t http_file_get_data(char*, size_t, off_t, char*);

typedef struct {
    char* data;
    size_t offset;
    size_t size;
} data;

#endif
