#include "network.h"
#ifdef WITH_CURL_SUPPORT
#include "http.h"
#endif

int net_file_get_info(char* url, lionfile_info_t* file_info) {
#ifdef WITH_CURL_SUPPORT
    if (strncmp("http", url, 4) == 0 || strncmp("ftp", url, 3) == 0) {
        return http_file_get_info(url, file_info);
    }
#endif
    return 1;
}

size_t net_file_get_data(char* url, size_t size, off_t off, char* buff) {
#ifdef WITH_CURL_SUPPORT
    if (strncmp("http", url, 4) == 0 || strncmp("ftp", url, 3) == 0) {
        return http_file_get_data(url, size, off, buff);
    }
#endif
    return 1;
}
