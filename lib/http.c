#include "http.h"

size_t write_callback(void *read_data, size_t size, size_t nmemb, void *p) {
    size_t real_size = size * nmemb;
    data* _data = (data*)p;
    // overflow
    if (_data->offset + real_size > _data->size) {
        fprintf(stderr, "Curl callback: skipping potential overflow\n");
        return 0;
    }
    memcpy((char *) _data->data + _data->offset, read_data, real_size);
    _data->offset += real_size;
    return real_size;
}

int http_file_get_info(char* url, lionfile_info_t* file_info) {
    CURL *curl = curl_easy_init();
    if (!curl) {
        fprintf(stderr, "Curl initialization failed when about to get length of %s\n", url);
        return 1;
    }
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_HEADER, 0);
    curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1); // redirect to location if there is

    CURLcode res = curl_easy_perform(curl);
    if (res != CURLE_OK) {
        fprintf(stderr, "Request to %s failed, reason: %s\n", url, curl_easy_strerror(res));
        return 1;
    }
    double content_length = 0;
    if(curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &content_length)) {
        curl_easy_cleanup(curl);
        return 1;
    }
    curl_easy_cleanup(curl);
    char tmp[30];
    sprintf(tmp, "%.0f", content_length);
    file_info->size = strtoll(tmp, NULL, 10);
    return 0;
}

size_t http_file_get_data(char* url, size_t size, off_t off, char* buff) {
    data _data;
    _data.data   = buff;
    _data.offset = 0;
    _data.size   = size;

    char buffer[128];
    CURL *curl;

    curl = curl_easy_init();
    if (!curl) {
        fprintf(stderr, "Curl initialization failed when about to get length of %s\n", url);
        return 0;
    }

    curl_easy_setopt(curl, CURLOPT_URL, url);

    snprintf(buffer, 128, "%ld-%ld", off, off+size-1);

    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt(curl, CURLOPT_RANGE, buffer);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&_data);

    // Perform the request
    CURLcode res = curl_easy_perform(curl);
    if (res != CURLE_OK) {
        fprintf(stderr, "Request to %s failed, reason: %s\n", url, curl_easy_strerror(res));
        return 0;
    }

    curl_easy_cleanup(curl);

    return _data.offset;
}
