/*
 * Remotefs
 * Copyright (c) 2020-* Zesty <zesty@mail.onion.bbox.wtf>
 *
 * lionfs, The Link Over Network File System
 * Copyright (C) 2016  Ricardo Biehl Pasquali <rbpoficial@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "remotefs.h"


lionfile_t **files;
pthread_rwlock_t rwlock;
char *file_cache;

void db_init() {
    sqlite3 *db;
    if(sqlite3_open(file_cache, &db)) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        exit(1);
    }
    sqlite3_exec(db,
            "create table if not exists file (\
            id integer primary key,\
            path text not null,\
            url text not null,\
            size text not null,\
            mode integer not null,\
            mtime text not null\
            )", NULL, 0, NULL);
    sqlite3_close(db);
}

lionfile_t *get_file_by_path(const char *path) {
    sqlite3 *db;
    sqlite3_stmt *pstmt;

    if(sqlite3_open(file_cache, &db)) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return NULL;
    }
    if(sqlite3_prepare_v3(db, "SELECT * from file where path = ?1;", -1, 0, &pstmt, NULL)) {
        fprintf(stderr, "Couldn't prepare sql statement: %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(pstmt);
        sqlite3_close_v2(db);
        return NULL;
    }

    if (sqlite3_bind_text(pstmt, 1, path, -1, NULL))
    {
        fprintf(stderr, "Couldn't bind to prepared sql stmt: %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(pstmt);
        sqlite3_close_v2(db);
        return NULL;
    }

    while(sqlite3_step(pstmt) == SQLITE_ROW)
    {
        lionfile_t* _new = malloc(sizeof(lionfile_t));
        _new->path 	= malloc(strlen((char*)sqlite3_column_text(pstmt, 1)));
        strcpy(_new->path, (char*)sqlite3_column_text(pstmt, 1));
        _new->url  	= malloc(strlen((char*)sqlite3_column_text(pstmt, 2)));
        strcpy(_new->url, (char*)sqlite3_column_text(pstmt, 2));
        _new->size 	= strtoll((const char*)sqlite3_column_text(pstmt, 3), NULL, 10);
        _new->mode 	= sqlite3_column_int(pstmt, 4);
        _new->mtime = strtol((const char*)sqlite3_column_text(pstmt, 5), NULL, 10);

        sqlite3_finalize(pstmt);
        sqlite3_close(db);
        return _new;
    }

    sqlite3_finalize(pstmt);
    sqlite3_close(db);
    return NULL;
}

lionfile_t *get_file_by_ff(const char *path) {
    if (strncmp(path, "/.ff/", 5) == 0) {
        path += 4;
        return get_file_by_path(path);
    }

    return NULL;
}

void remove_file_by_path(const char *path) {
    sqlite3 *db;
    sqlite3_stmt *pstmt;

    if(sqlite3_open(file_cache, &db)) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return;
    }
    if(sqlite3_prepare_v3(db, "delete from file where path = ?1;", -1, 0, &pstmt, NULL)) {
        fprintf(stderr, "Couldn't prepare sql statement: %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(pstmt);
        sqlite3_close_v2(db);
        return;
    }

    if (sqlite3_bind_text(pstmt, 1, path, -1, NULL))
    {
        fprintf(stderr, "Couldn't bind to prepared sql stmt: %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(pstmt);
        sqlite3_close_v2(db);
        return;
    }

    sqlite3_step(pstmt);

    sqlite3_finalize(pstmt);
    sqlite3_close(db);

    return;
}

void add_file_sym(const lionfile_t* file) {
    sqlite3 *db;
    sqlite3_stmt *pstmt;

    if(sqlite3_open(file_cache, &db)) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return;
    }
    if(sqlite3_prepare_v3(db, "insert into file (path, url, size, mode, mtime) values (?1, ?2, ?3, ?4, ?5);", -1, 0, &pstmt, NULL)) {
        fprintf(stderr, "Couldn't prepare sql statement: %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(pstmt);
        sqlite3_close_v2(db);
        return;
    }

    char size[50];
    char mtime[30];
    sprintf(size, "%lld", file->size);
    sprintf(mtime, "%ld", file->mtime);

    if (sqlite3_bind_text(pstmt, 1, file->path, -1, NULL)
            || sqlite3_bind_text(pstmt, 2, file->url, -1, NULL)
            || sqlite3_bind_text(pstmt, 3, size, -1, NULL)
            || sqlite3_bind_int(pstmt, 4, file->mode)
            || sqlite3_bind_text(pstmt, 5, mtime, -1, NULL)) {
        fprintf(stderr, "Couldn't bind to prepared sql stmt: %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(pstmt);
        sqlite3_close_v2(db);
        return;
    }

    sqlite3_step(pstmt);

    sqlite3_finalize(pstmt);
    sqlite3_close(db);

    return;
}

// ================
// fuse operations:
//   lion_getattr()   get attributes (information) from a file
//   lion_readlink()  get target of a symbolic link (or get the fakefile ...)
//   lion_unlink()    removes a file -- only symlinks in lionfs :-)
//   lion_symlink()   creates a symlink
//   lion_rename()    renames a file -- only symlinks as lion_unlink()
//   lion_read()      reads content of a file (reads content of fakefiles)
//   lion_readdir()   get files in a directory (get symlinks in lionfs array)
// ================

static int lion_getattr(const char *path, struct stat *buf) {
    lionfile_t *file;

    memset(buf, 0, sizeof(struct stat));

    /* is it our root directory? */
    if (strcmp(path, "/") == 0) {
        buf->st_mode = S_IFDIR | 0775;
        buf->st_nlink = 2;
        return 0;
    }

    /* is it fakefiles directory? */
    if (strcmp(path, "/.ff") == 0) {
        /* fakefiles directory needs to be read-only */
        buf->st_mode = S_IFDIR | 0444;
        buf->st_nlink = 0;
        return 0;
    }

    pthread_rwlock_rdlock(&rwlock); /* read lock */

    /* is it a fakefile! */
    if ((file = get_file_by_ff(path)) != NULL) {
        buf->st_mode = S_IFREG | 0444;
        buf->st_mtime = file->mtime;
        buf->st_nlink = 0;
        buf->st_size = file->size;
        free(file->path);
        free(file->url);
        free(file);
        pthread_rwlock_unlock(&rwlock);
        return 0;
    }

    /* doesn't symlink exist? */
    if ((file = get_file_by_path(path)) == NULL) {
        pthread_rwlock_unlock(&rwlock);
        return -ENOENT;
    }
    /* If symlink exists we do this:  */
    buf->st_mode = file->mode | S_IFLNK; /* S_IFLNK = symlink bitmask */
    buf->st_mtime = file->mtime;         /* modification time */
    buf->st_nlink = 1; /*number of hard links (here it's not so important)*/
    buf->st_size = file->size;

    free(file);

    pthread_rwlock_unlock(&rwlock);

    return 0;
}

static int lion_readlink(const char *path, char *buf, size_t len) {
    lionfile_t *file;

    pthread_rwlock_rdlock(&rwlock); /* read lock */

    /* doesn't symlink exist? */
    if ((file = get_file_by_path(path)) == NULL) {
        pthread_rwlock_unlock(&rwlock);
        return -ENOENT;
    }

    snprintf(buf, len, ".ff/%s", file->path + 1);

    free(file->path);
    free(file->url);
    free(file);

    pthread_rwlock_unlock(&rwlock);

    return 0;
}

static int lion_unlink(const char *path) {
    lionfile_t *file;

    pthread_rwlock_wrlock(&rwlock); /* write lock */

    /* doesn't symlink exist? */
    if ((file = get_file_by_path(path)) == NULL) {
        pthread_rwlock_unlock(&rwlock);
        return -ENOENT;
    }

    remove_file_by_path(path);

    free(file->path);
    free(file->url);
    free(file);

    pthread_rwlock_unlock(&rwlock);

    return 0;
}

static int lion_symlink(const char *url, const char *path) {
    lionfile_info_t file_info;
    lionfile_t *file;

    /* check if URL exists and get its info */
    if (net_file_get_info((char*) url, &file_info))
        return -EHOSTUNREACH;

    pthread_rwlock_wrlock(&rwlock); /* write lock */

    /* does symlink already exist? */
    if ((file = get_file_by_path(path)) != NULL) {
        free(file->path);
        free(file->url);
        free(file);
        pthread_rwlock_unlock(&rwlock);
        return -EEXIST;
    }

    file = malloc(sizeof(lionfile_t));
    file->path = malloc(strlen(path) + 1);
    file->url = malloc(strlen(url) + 1);

    strcpy(file->path, path);
    strcpy(file->url, url);

    /* symlinks are read-only :-) -- fakefiles copy this */
    file->mode = 0444;
    file->size = file_info.size;
    time(&file->mtime);

    add_file_sym(file);

    free(file->path);
    free(file->url);
    free(file);

    pthread_rwlock_unlock(&rwlock);

    return 0;
}

static int lion_rename(const char *oldpath, const char *newpath) {
    lionfile_t *file;
    char **path_pointer;
    size_t newsize;

    pthread_rwlock_wrlock(&rwlock); /* write lock */

    /* does new-symlink-name already exist? */
    if (get_file_by_path(newpath) != NULL) {
        pthread_rwlock_unlock(&rwlock);
        return -EEXIST;
    }
    /* doesn't old-symlink-name exist? */
    if ((file = get_file_by_path(oldpath)) == NULL) {
        pthread_rwlock_unlock(&rwlock);
        return -ENOENT;
    }

    path_pointer = &file->path;
    newsize = strlen(newpath) + 1;

    if (newsize == 1) {
        pthread_rwlock_unlock(&rwlock);
        return -EINVAL;
    }

    *path_pointer = realloc(*path_pointer, newsize);

    memcpy(*path_pointer, newpath, newsize);

    pthread_rwlock_unlock(&rwlock);

    return 0;
}

static int lion_read(const char *path, char *buf, size_t size, off_t off,
        struct fuse_file_info *fi) {
    lionfile_t *file;
    size_t ret = 0;

    pthread_rwlock_rdlock(&rwlock); /* read lock */

    /* doesn't symlink exist? */
    if ((file = get_file_by_ff(path)) == NULL) {
        pthread_rwlock_unlock(&rwlock);
        return -ENOENT;
    }

    if (off < file->size) {
        if (off + size > file->size)
            size = file->size - off;
    } else {
        pthread_rwlock_unlock(&rwlock);
        return 0;
    }

    ret = net_file_get_data(file->url, size, off, buf);
    if (ret == 0) {
        fprintf(stderr, "Error reading %s\n", file->url);
    }

    free(file->path);
    free(file->url);
    free(file);

    pthread_rwlock_unlock(&rwlock);

    return ret;
}

static int lion_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
        off_t off, struct fuse_file_info *fi) {

    /* doesn't path equals "/" */
    if (strcmp(path, "/") != 0)
        return -ENOENT;

    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);

    pthread_rwlock_rdlock(&rwlock); /* read lock */


    sqlite3 *db;
    sqlite3_stmt *pstmt;

    if(sqlite3_open(file_cache, &db)) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return -2;
    }
    if(sqlite3_prepare_v3(db, "SELECT * from file;", -1, 0, &pstmt, NULL)) {
        fprintf(stderr, "Couldn't prepare sql statement: %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(pstmt);
        sqlite3_close_v2(db);
        return -2;
    }

    while(sqlite3_step(pstmt) == SQLITE_ROW)
    {
        printf("found something\n");
        filler(buf, (char*)sqlite3_column_text(pstmt, 1) + 1, NULL, 0);
    }

    sqlite3_finalize(pstmt);
    sqlite3_close(db);

    pthread_rwlock_unlock(&rwlock);

    return 0;
}

static struct fuse_operations fuseopr = {
    .getattr = lion_getattr,
    .readlink = lion_readlink,
    .unlink = lion_unlink,
    .symlink = lion_symlink,
    .rename = lion_rename,
    .read = lion_read,
    .readdir = lion_readdir,
};

int main(int argc, char **argv) {
    int ret = 0;

    // init rwlock
    if (pthread_rwlock_init(&rwlock, NULL))
        return 1;

    // check if files already exist in same path
    char *dir_name = argv[argc - 1];
    char *cache = "_cache.db";
    file_cache = malloc(strlen(dir_name) + strlen(cache) + 1);
    strcpy(file_cache, dir_name);
    strcat(file_cache, cache);
    db_init();

    // Main routine. It initializes FUSE and set the operations (&fuseopr).
    fuse_main(argc, argv, &fuseopr, NULL);


_go_destroy_rwlock:
    // destroy rwlock
    pthread_rwlock_destroy(&rwlock);

    return ret;
}
