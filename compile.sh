cwd="`pwd`"
xdir="`dirname $0`"

cd $xdir

cc -DWITH_CURL_SUPPORT=1 -D_FILE_OFFSET_BITS=64 -lfuse -ldl -lpthread -lsqlite3 -lcurl -o remotefs remotefs.c lib/network.c lib/http.c


cd $cwd